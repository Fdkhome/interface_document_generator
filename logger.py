import os, sys
import time
import logging.handlers

program_root_path = os.path.dirname(os.path.realpath(sys.argv[0]))
log_file_path = os.path.join(program_root_path, 'logs')
if not os.path.exists(log_file_path):
    os.makedirs(log_file_path)
log_dir_path = log_file_path
# logging初始化工作
# time_stamp_name = time.strftime("%Y%m%d", time.localtime())
# log_name = time_stamp_name + ".log"
log_name = "records.log"
Info_path = os.path.join(log_dir_path, log_name)
log_format = "%(asctime)s --%(name)s -- ProcessID:%(process)d -- ThreadName:%(threadName)s(ID:%(thread)d) %(filename)s.%(funcName)s():(%(funcName)s:%(lineno)d) [%(levelname)s]:\n%(message)s"
date_format = "%Y-%m-%d %H:%M:%S %a"
logging.basicConfig(level=logging.DEBUG,
                    format=log_format,
                    datefmt=date_format,
                    )
# myapp的初始化工作
logger = logging.getLogger('all_log_info')

# 写入文件，如果文件超过10M，仅保留15个文件。

handler2 = logging.handlers.RotatingFileHandler(filename=Info_path, maxBytes=1024 * 10 * 1024, backupCount=15,
                                                encoding="utf-8", delay=False)
fmt = logging.Formatter(fmt=log_format)
handler2.setFormatter(fmt)
handler2.setLevel(logging.INFO)
# 设置后缀名称，跟strftime的格式一样
logger.addHandler(handler2)

# 调用样例

# 导入logger
# from common.logger import logger

# 基础打印
# logger.info("123456")

# 异常出错打印
# try:
#     a = b
# except BaseException as e:
#     logger.error(e, exc_info=1)
