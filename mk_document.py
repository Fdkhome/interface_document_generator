import json


def funcB(tdict, tlist):
    for i in tdict:
        if isinstance(i, (list, tuple)):
            funcB(i, tlist)
        elif isinstance(i, dict):
            funcA(i, tlist)


def funcA(tdict, tlist):
    for i in list(tdict.keys()):
        if (i, str(type(tdict[i]))) not in tlist:
            tlist.append((i, str(type(tdict[i]))))
        if isinstance(tdict[i], dict):
            funcA(tdict[i], tlist)
        elif isinstance(tdict[i], (list, tuple)):
            funcB(tdict[i], tlist)

    return tlist


def mk_tabel(data):
    if data != '' and data != None:
        # data = eval(data)
        if isinstance(data, str):
            data = eval(data)
        # print(data)
        # print(type(data))
        res_data = funcA(data, [])
        # print(res_data)
        # res_data = list(set(res_data))
        # print(res_data)
        # print('类型提取')
        d = '''
        |参数名|参数类型|说明|
        |---|---|---|
        '''.replace(' ', '')
        for i in res_data:

            d += '|%s|%s|%s|\n'%(i[0],i[1][8:-2],'')
        return d
    # print(d)
    return ''


def mk_head(req_head):
    if len(req_head) > 0:
        d = '''
        **请求头参数说明:**
                |key|value|
                |---|---|
                '''.replace(' ', '')
        for k, v in req_head.items():
            d += '|%s|%s|\n' % (k, v)
        return d
    else:
        return ''


text_ = '''
**描述:** %s

**接口地址:** %s

**请求方式:** %s

**数据类型:** %s
%s
%s
**请求示例:**
```json
%s
```
%s%s
**返回示例：**
```json
%s
```

    '''


def mk_document(info_data):
    # print('111:', type(req_data), req_data)
    # print('222:', type(res_data), res_data)
    # print(info_data)
    # print(info_data.get('res_data'))
    # print(type(info_data.get('res_data')))
    req_data = info_data['req_data']
    res_data = info_data['res_data']

    # if info_data.get('res_data_type') == 'json':
    #     pass
    # else:
    #     pass
    try:
        req_tabel = mk_tabel(req_data)
        if req_data == '' or req_data == None:
            req_data = '没有额外请求参数'
        elif req_tabel:
            req_tabel = '**请求参数说明:**\n' + req_tabel
            req_data = eval(str(req_data))
            req_data = json.dumps(req_data, indent=4, ensure_ascii=False, separators=(',', ': '))

        if info_data.get('res_data_type') == 'json':
            res_tabel = mk_tabel(res_data)
                # print('111')
            res_data = json.dumps(info_data['res_data'], indent=4, ensure_ascii=False, separators=(',', ': '))
            res_head= "**返回参数说明:**："
        else:
            res_tabel = ''
            res_head = ""
            res_data = "Content-Type:" + info_data.get('res_data_type')
            # res_head= ""
            # pass
        a = text_ % (info_data['des'], info_data['url'], info_data['method'], 'json', mk_head(info_data['req_head']), req_tabel, req_data, res_head, res_tabel,  res_data)
        # print(a)
        return a
    except Exception as e:
        print('复制文档失败：', e)
